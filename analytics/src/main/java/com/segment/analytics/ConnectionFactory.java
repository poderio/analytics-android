/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Segment.io, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.segment.analytics;

import android.util.Base64;
import com.segment.analytics.core.BuildConfig;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.segment.analytics.internal.Utils.isNullOrEmpty;

/**
 * Abstraction to customize how connections are created. This is can be used to point our SDK at
 * your proxy server for instance.
 */
public class ConnectionFactory {

  private final String BASE_URL;
  private static final String PRODUCTION_URL = "https://pixel.epica.ai";
  private static final String STAGING_URL = "https://pixel.staging.epica.ai";
  private static final int DEFAULT_READ_TIMEOUT_MILLIS = 20 * 1000; // 20s
  private static final int DEFAULT_CONNECT_TIMEOUT_MILLIS = 15 * 1000; // 15s
  static final String USER_AGENT = "analytics-android/" + BuildConfig.VERSION_NAME;

  /** Start building a new {@link ConnectionFactory} instance. */
  public ConnectionFactory() {
    this.BASE_URL = PRODUCTION_URL;
  }

  /** Start building a new {@link ConnectionFactory} instance. */
  public ConnectionFactory(String environment) {
    if (isNullOrEmpty(environment)) {
      throw new IllegalArgumentException("Environment can not be null or empty.");
    }
    if (environment.equals(Analytics.STAGING_ENVIRONMENT)) {
      this.BASE_URL = STAGING_URL;
    } else {
      this.BASE_URL = PRODUCTION_URL;
    }
  }

  private String authorizationHeader(String writeKey) {
    return "Basic " + Base64.encodeToString((writeKey + ":").getBytes(), Base64.NO_WRAP);
  }

  /** Return a {@link HttpURLConnection} that reads JSON formatted project settings. */
  public HttpURLConnection projectSettings(String writeKey) throws IOException {
    return openConnection(BASE_URL + "/api/v1/projects/" + writeKey + "/settings");
  }

  /**
   * Return a {@link HttpURLConnection} that writes batched payloads to {@code
   * https://pixel.epica.ai/api/v1/android}.
   */
  public HttpURLConnection upload(String writeKey) throws IOException {
    HttpURLConnection connection = openConnection(BASE_URL + "/api/v1/android");
    connection.setRequestProperty("Authorization", authorizationHeader(writeKey));
    connection.setRequestProperty("Content-Encoding", "gzip");
    connection.setDoOutput(true);
    connection.setChunkedStreamingMode(0);
    return connection;
  }

  /**
   * Return a {@link HttpURLConnection} that writes gets attribution information from {@code
   * https://pixel.epica.ai/api/v1/attribution}.
   */
  public HttpURLConnection attribution(String writeKey) throws IOException {
    HttpURLConnection connection = openConnection(BASE_URL + "/api/v1/attribution");
    connection.setRequestProperty("Authorization", authorizationHeader(writeKey));
    connection.setRequestMethod("POST");
    connection.setDoOutput(true);
    return connection;
  }

  /**
   * Configures defaults for connections opened with {@link #upload(String)}, {@link
   * #attribution(String)} and {@link #projectSettings(String)}.
   */
  protected HttpURLConnection openConnection(String url) throws IOException {
    URL requestedURL;

    try {
      requestedURL = new URL(url);
    } catch (MalformedURLException e) {
      throw new IOException("Attempted to use malformed url: " + url, e);
    }

    HttpURLConnection connection = (HttpURLConnection) requestedURL.openConnection();
    connection.setConnectTimeout(DEFAULT_CONNECT_TIMEOUT_MILLIS);
    connection.setReadTimeout(DEFAULT_READ_TIMEOUT_MILLIS);
    connection.setRequestProperty("Content-Type", "application/json");
    connection.setRequestProperty("User-Agent", USER_AGENT);
    connection.setDoInput(true);
    return connection;
  }
}
