package com.segment.analytics.recommender;

import java.util.List;

public class RecommendedProduct implements RecommendedItem {

    private String id;
    private String sku;
    private String status;
    private Double score;
    private String title;
    private String brand;
    private List<String> categories;
    private List<String> tags;
    private String description;
    private String price;
    private String discount;
    private String discountPercent;
    private String image;
    private String url;
    private String createdAt;
    private String updatedAt;

    RecommendedProduct(String id, String sku, String status, Double score, String title,
                       String brand, List<String> categories, List<String> tags,
                       String description, String price, String discount,
                       String discountPercent, String image, String url,
                       String createdAt, String updatedAt) {
        this.id = id;
        this.sku = sku;
        this.status = status;
        this.score = score;
        this.title = title;
        this.brand = brand;
        this.categories = categories;
        this.tags = tags;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.discountPercent = discountPercent;
        this.image = image;
        this.url = url;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }

    public String getStatus() {
        return status;
    }

    public Double getScore() {
        return score;
    }

    public String getTitle() {
        return title;
    }

    public String getBrand() {
        return brand;
    }

    public List<String> getCategories() {
        return categories;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public String getDiscount() {
        return discount;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public String getImage() {
        return image;
    }

    public String getUrl() {
        return url;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }
}
