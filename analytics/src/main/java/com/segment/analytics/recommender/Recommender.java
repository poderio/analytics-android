package com.segment.analytics.recommender;

import static com.segment.analytics.internal.Utils.isNullOrEmpty;

/**
 * Create a {@link Recommender} instance. This is used to point to our recommender API.
 */
public class Recommender {

    private final String DEVICE_ID, WRITE_KEY, ENVIRONMENT;

    /** Start building a new {@link Recommender} instance. */
    public Recommender(String writeKey, String environment) {
        this(writeKey, environment, null);
    }

    /** Start building a new {@link Recommender} instance. */
    public Recommender(String writeKey, String environment, String deviceId) {
        if (isNullOrEmpty(writeKey)) {
            throw new IllegalArgumentException("WriteKey can not be null or empty.");
        }
        if (isNullOrEmpty(environment)) {
            throw new IllegalArgumentException("Environment can not be null or empty.");
        }
        this.DEVICE_ID = deviceId;
        this.WRITE_KEY = writeKey;
        this.ENVIRONMENT = environment;
    }

    /**
     * Return a {@link RecommenderTemplateClient} to make requests to the recommender API
     * via template ID.
     */
    public RecommenderTemplateClient getByTemplateId(int templateId) {
        return new RecommenderTemplateClient(DEVICE_ID, WRITE_KEY, ENVIRONMENT, templateId);
    }

    /**
     * Return a {@link RecommenderCollectionClient} to make requests to the recommender API
     * via collection ID.
     */
    public RecommenderCollectionClient getByCollectionId(int collectionId) {
        return new RecommenderCollectionClient(DEVICE_ID, WRITE_KEY, ENVIRONMENT, collectionId);
    }
}
