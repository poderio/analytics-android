package com.segment.analytics.recommender;

/**
 * Abstraction to customize how to handle requests to the recommender API.
 */
abstract class RecommenderCallback {

    public abstract void onFailure(Throwable t);
}
