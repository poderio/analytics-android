package com.segment.analytics.recommender;

import androidx.annotation.Nullable;

import com.segment.analytics.Analytics;
import com.segment.analytics.core.BuildConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.segment.analytics.internal.Utils.buffer;

/**
 * Abstraction to customize how connections are created to work with recommender API
 * and to represent JSON responses.
 */
abstract class RecommenderClient {

    private final String DEVICE_ID, WRITE_KEY, BASE_URL;
    private static final String PRODUCTION_URL = "https://api.epica.ai/api/v1/recommender";
    private static final String STAGING_URL = "https://api.staging.epica.ai/api/v1/recommender";
    private static final String COLLECTION_SOURCE = "collections";
    private static final String TEMPLATE_SOURCE = "templates";
    private static final int DEFAULT_READ_TIMEOUT_MILLIS = 20 * 1000; // 20s
    private static final int DEFAULT_CONNECT_TIMEOUT_MILLIS = 15 * 1000; // 15s
    private static final String USER_AGENT = "analytics-android/" + BuildConfig.VERSION_NAME;

    /** Start building a new {@link RecommenderClient} instance. */
    RecommenderClient(String deviceId, String writeKey, String environment) {
        this.DEVICE_ID = deviceId;
        this.WRITE_KEY = writeKey;
        if (environment.equals(Analytics.STAGING_ENVIRONMENT)) {
            this.BASE_URL = STAGING_URL;
        } else {
            this.BASE_URL = PRODUCTION_URL;
        }
    }

    private List<String> JSONArrayToList(@Nullable JSONArray array) {
        if (array == null) {
            return Collections.emptyList();
        } else {
            List<String> list = new ArrayList<>(array.length());
            for (int i = 0; i < array.length(); i++) {
                list.add(array.optString(i, null));
            }
            return list;
        }
    }

    private JSONObject inputStreamToJson(InputStream inputStream) throws IOException, JSONException {
        BufferedReader reader = buffer(inputStream);
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        return new JSONObject(builder.toString());
    }

    private RecommenderTemplate getTemplateFromJSON(JSONObject source) throws JSONException {
        JSONArray buckets = source.getJSONArray("buckets");
        JSONObject template = buckets.getJSONObject(0);
        JSONArray items = template.getJSONArray("items");
        List<RecommendedProduct> itemsList = new ArrayList<>(items.length());
        for (int i = 0; i < items.length(); i++) {
            JSONObject item = items.getJSONObject(i);

            RecommendedProduct product = new RecommendedProduct(
                    item.optString("id"),
                    item.optString("sku"),
                    item.optString("status"),
                    item.optDouble("score"),
                    item.optString("title"),
                    item.optString("brand", null),
                    JSONArrayToList(item.optJSONArray("categories")),
                    JSONArrayToList(item.optJSONArray("tags")),
                    item.optString("description", null),
                    item.optString("price", null),
                    item.optString("discount", null),
                    item.optString("discount_percent", null),
                    item.optString("image", null),
                    item.optString("url"),
                    item.optString("created_at"),
                    item.optString("updated_at")
            );
            itemsList.add(product);
        }
        return new RecommenderTemplate(
                template.optString("basic_persona_id", null),
                template.optString("segment", null),
                template.optInt("template_id"),
                template.optString("title", null),
                template.optInt("priority"),
                itemsList);
    }

    private RecommenderCollection getCollectionFromJSON(JSONObject source) throws JSONException {
        JSONArray buckets = source.getJSONArray("buckets");
        List<RecommenderTemplate> templateList = new ArrayList<>(buckets.length());
        for (int i = 0; i < buckets.length(); i++) {
            templateList.add(getTemplateFromJSON(buckets.getJSONObject(i)));
        }
        return new RecommenderCollection(
                source.optString("write_key"),
                source.optInt("collection_id"),
                templateList);
    }

    /** Attach a payload to {@link HttpURLConnection}. */
    private void sendPayload(HttpURLConnection connection) throws IOException {
        byte[] payload = String.format("{\"device_id\":\"%s\"}", DEVICE_ID).getBytes();
        connection.setFixedLengthStreamingMode(payload.length);
        connection.connect();
        try(OutputStream os = connection.getOutputStream()) {
            os.write(payload);
        }
    }

    /**
     * Configures defaults for connections opened with {@link #getRecommenderTemplate(int)}
     * and {@link #getRecommenderCollection(int)}.
     */
    private HttpURLConnection openConnection(String source, int id) throws IOException {
        URL requestedURL;
        String url = String.format("%s/%s/products/%s/%s", BASE_URL, WRITE_KEY, source, id);

        try {
            requestedURL = new URL(url);
        } catch (MalformedURLException e) {
            throw new IOException("Attempted to use malformed url: " + url, e);
        }

        HttpURLConnection connection = (HttpURLConnection) requestedURL.openConnection();
        connection.setConnectTimeout(DEFAULT_CONNECT_TIMEOUT_MILLIS);
        connection.setReadTimeout(DEFAULT_READ_TIMEOUT_MILLIS);
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        return connection;
    }

    /** Return a {@link RecommenderTemplate} object that represents a JSON response. */
    RecommenderTemplate getRecommenderTemplate(int templateId) throws IOException, JSONException {
        HttpURLConnection connection = openConnection(TEMPLATE_SOURCE, templateId);
        sendPayload(connection);
        try(InputStream inputStream = connection.getInputStream()) {
            return getTemplateFromJSON(inputStreamToJson(inputStream));
        }
    }

    /** Return a {@link RecommenderCollection} object that represents a JSON response. */
    RecommenderCollection getRecommenderCollection(int collectionId) throws IOException, JSONException {
        HttpURLConnection connection = openConnection(COLLECTION_SOURCE, collectionId);
        sendPayload(connection);
        try(InputStream inputStream = connection.getInputStream()) {
            return getCollectionFromJSON(inputStreamToJson(inputStream));
        }
    }
}
