package com.segment.analytics.recommender;

import java.util.List;

public class RecommenderCollection {

    private String writeKey;
    private Integer collectionId;
    private List<RecommenderTemplate> recommenderTemplates;

    RecommenderCollection(String writeKey, Integer collectionId,
                          List<RecommenderTemplate> recommenderTemplates) {
        this.writeKey = writeKey;
        this.collectionId = collectionId;
        this.recommenderTemplates = recommenderTemplates;
    }

    public String getWriteKey() {
        return writeKey;
    }

    public Integer getCollectionId() {
        return collectionId;
    }

    public List<RecommenderTemplate> getRecommenderTemplates() {
        return recommenderTemplates;
    }
}
