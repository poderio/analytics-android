package com.segment.analytics.recommender;

/**
 * Abstraction to customize how to handle requests to the recommender API.
 */
public abstract class RecommenderCollectionCallback extends RecommenderCallback {

    public abstract void onSuccess(RecommenderCollection collection);
}
