package com.segment.analytics.recommender;

import android.os.Handler;
import android.os.Looper;

import org.json.JSONException;

import java.io.IOException;

public class RecommenderCollectionClient extends RecommenderClient {

    private final int COLLECTION_ID;

    /** Start building a new {@link RecommenderCollectionClient} instance. */
    RecommenderCollectionClient(String deviceId, String writeKey, String environment,
                                int collectionId) {
        super(deviceId, writeKey, environment);
        this.COLLECTION_ID = collectionId;
    }

    /** Return a {@link RecommenderCollection} synchronously. */
    public RecommenderCollection sync() throws IOException, JSONException {
        return getRecommenderCollection(COLLECTION_ID);
    }

    /** Return a {@link RecommenderCollection} asynchronously. */
    public void async(RecommenderCollectionCallback callback) {
        Handler handler = new Handler(Looper.getMainLooper());
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    RecommenderCollection collection = getRecommenderCollection(COLLECTION_ID);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onSuccess(collection);
                        }
                    });
                } catch (Exception e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFailure(e);
                        }
                    });
                }
            }
        }).start();
    }
}
