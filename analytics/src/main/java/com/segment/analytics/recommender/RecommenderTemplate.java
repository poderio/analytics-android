package com.segment.analytics.recommender;

import java.util.List;

public class RecommenderTemplate {

    private String basicPersonaId;
    private String segment;
    private Integer templateId;
    private String title;
    private Integer priority;
    private List<RecommendedProduct> recommendedProducts;

    RecommenderTemplate(String basicPersonaId, String segment, Integer templateId,
                        String title, Integer priority,
                        List<RecommendedProduct> recommendedProducts) {
        this.basicPersonaId = basicPersonaId;
        this.segment = segment;
        this.templateId = templateId;
        this.title = title;
        this.priority = priority;
        this.recommendedProducts = recommendedProducts;
    }

    public String getBasicPersonaId() {
        return basicPersonaId;
    }

    public String getSegment() {
        return segment;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public String getTitle() {
        return title;
    }

    public Integer getPriority() {
        return priority;
    }

    public List<RecommendedProduct> getRecommendedProducts() {
        return recommendedProducts;
    }
}
