package com.segment.analytics.recommender;

/**
 * Abstraction to customize how to handle requests to the recommender API.
 */
public abstract class RecommenderTemplateCallback extends RecommenderCallback {

    public abstract void onSuccess(RecommenderTemplate template);
}
