package com.segment.analytics.recommender;

import android.os.Handler;
import android.os.Looper;

import org.json.JSONException;

import java.io.IOException;

public class RecommenderTemplateClient extends RecommenderClient {

    private final int TEMPLATE_ID;

    /** Start building a new {@link RecommenderTemplateClient} instance. */
    RecommenderTemplateClient(String deviceId, String writeKey, String environment,
                              int templateId) {
        super(deviceId, writeKey, environment);
        this.TEMPLATE_ID = templateId;
    }

    /** Return a {@link RecommenderTemplate} synchronously. */
    public RecommenderTemplate sync() throws IOException, JSONException {
        return getRecommenderTemplate(TEMPLATE_ID);
    }

    /** Return a {@link RecommenderTemplate} asynchronously. */
    public void async(RecommenderTemplateCallback callback) {
        Handler handler = new Handler(Looper.getMainLooper());
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    RecommenderTemplate template = getRecommenderTemplate(TEMPLATE_ID);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onSuccess(template);
                        }
                    });
                } catch (Exception e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFailure(e);
                        }
                    });
                }
            }
        }).start();
    }
}
